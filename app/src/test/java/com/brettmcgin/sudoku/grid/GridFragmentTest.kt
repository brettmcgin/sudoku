package com.brettmcgin.sudoku.grid

import android.os.Looper.getMainLooper
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf

@RunWith(AndroidJUnit4::class)
class GridFragmentTest {

    @MockK
    lateinit var gridViewModel: GridViewModel

    private val scenario by lazy { launchFragmentInContainer<GridFragment>() }

    @Before
    fun setup() {
        MockKAnnotations.init(this)

        every { gridViewModel.repository } returns MockGridRepository()
    }

    @Test
    fun `Cell onClickEventHandler`() {
        scenario.onFragment { fragment ->
            fragment.binding.grid.groupTopLeft.topLeft.cell.performClick()

            shadowOf(getMainLooper()).idle()

            assertEquals("10", fragment.binding.grid.groupTopLeft.topLeft.cell.text)
        }
    }
}
