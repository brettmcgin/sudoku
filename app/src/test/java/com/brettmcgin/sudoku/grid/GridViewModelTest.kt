package com.brettmcgin.sudoku.grid

import android.os.Looper
import androidx.fragment.app.Fragment
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.brettmcgin.sudoku.annotation.ApplicationScope
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows
import toothpick.testing.ToothPickRule
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class GridViewModelTest {

    private val toothPickRule = ToothPickRule(this)
        @Rule get

    @MockK
    lateinit var gridViewModel: GridViewModel

    val scenario by lazy { launchFragmentInContainer<Fragment>() }

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        toothPickRule.setScopeName(ApplicationScope::class.java)
        toothPickRule.inject(this)

        every { gridViewModel.repository } returns MockGridRepository()
    }

    @Test
    fun `Answer Event Updates LiveData`() {
        val gridViewModel = GridViewModel(MockGridRepository())

        gridViewModel.answer(GridEvent.AnswerEvent(10, Pair(3, 7)))

        Shadows.shadowOf(Looper.getMainLooper()).idle()

        assertEquals(10, gridViewModel.liveData.getOrAwaitValue().grid.answers[3][7])
    }

    fun <T> LiveData<T>.getOrAwaitValue(
        time: Long = 2,
        timeUnit: TimeUnit = TimeUnit.SECONDS
    ): T {
        var data: T? = null
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(o: T?) {
                data = o
                latch.countDown()
                this@getOrAwaitValue.removeObserver(this)
            }
        }

        this.observeForever(observer)

        // Don't wait indefinitely if the LiveData is not set.
        if (!latch.await(time, timeUnit)) {
            throw TimeoutException("LiveData value was never set.")
        }

        @Suppress("UNCHECKED_CAST")
        return data as T
    }
}
