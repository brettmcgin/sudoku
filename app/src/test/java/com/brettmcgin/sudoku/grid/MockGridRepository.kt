package com.brettmcgin.sudoku.grid

import toothpick.InjectConstructor

@InjectConstructor
class MockGridRepository : GridRepository {
    private val row = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
    override suspend fun getPuzzle(difficulty: Difficulty) = Grid(
        answers = arrayOf(row, row, row, row, row, row, row, row, row)
    )
}
