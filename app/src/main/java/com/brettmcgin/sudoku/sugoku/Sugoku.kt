package com.brettmcgin.sudoku.sugoku

import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface Sugoku {
    @GET("board")
    @Headers("Content-Type: application/json")
    suspend fun fetchPuzzle(
        @Query("difficulty") difficulty: String,
    ): SugokuResponse
}

data class SugokuResponse(
    val board: List<List<Int>>
)
