package com.brettmcgin.sudoku.sugoku

import com.brettmcgin.sudoku.annotation.ApplicationScope
import com.brettmcgin.sudoku.grid.Difficulty
import com.brettmcgin.sudoku.grid.Grid
import com.brettmcgin.sudoku.grid.GridRepository
import toothpick.InjectConstructor

@ApplicationScope
@InjectConstructor
class SugokuRepository : GridRepository {
    private val client = SugokuClient()

    override suspend fun getPuzzle(difficulty: Difficulty) = Grid(
        answers = client.getPuzzle(difficulty).board.map { row -> row.toTypedArray() }
            .toTypedArray()
    )
}
