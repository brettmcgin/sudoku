package com.brettmcgin.sudoku.sugoku

import android.annotation.SuppressLint
import com.brettmcgin.sudoku.grid.Difficulty
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val BASE_URL = "https://sugoku.herokuapp.com"
private const val CONNECTION_TIMEOUT_MS: Long = 10

class SugokuClient {
    private val client: Sugoku by lazy {
        Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(
                OkHttpClient.Builder()
                    .connectTimeout(CONNECTION_TIMEOUT_MS, TimeUnit.SECONDS)
                    .build()
            )
            .build()
            .create(Sugoku::class.java)
    }

    @SuppressLint("DefaultLocale")
    suspend fun getPuzzle(difficulty: Difficulty = Difficulty.MEDIUM): SugokuResponse =
        client.fetchPuzzle(difficulty.toString().toLowerCase())
}
