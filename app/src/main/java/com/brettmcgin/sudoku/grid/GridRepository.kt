package com.brettmcgin.sudoku.grid

interface GridRepository {
    suspend fun getPuzzle(difficulty: Difficulty = Difficulty.MEDIUM): Grid
}
