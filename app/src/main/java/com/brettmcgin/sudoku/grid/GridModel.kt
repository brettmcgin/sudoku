package com.brettmcgin.sudoku.grid

data class GridModel(
    val answers: Array<Array<Int>> = arrayOf(arrayOf()),
    val hints: Array<Array<Hint>> = arrayOf(arrayOf())
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GridModel

        if (!answers.contentDeepEquals(other.answers)) return false

        return true
    }

    override fun hashCode(): Int {
        return answers.contentDeepHashCode()
    }
}
