package com.brettmcgin.sudoku.grid

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import toothpick.InjectConstructor

@InjectConstructor
open class GridViewModel(@VisibleForTesting val repository: GridRepository) : ViewModel() {
    private val mutableViewState = MutableLiveData<GridViewState>()
    val liveData: LiveData<GridViewState> = mutableViewState

    private var grid = Grid()
    private var hints = arrayListOf(arrayListOf<Hint>())

    data class GridViewState(
        val grid: Grid,
        val hints: ArrayList<ArrayList<Hint>>
    )

    init {
        load()
    }

    fun load() {
        viewModelScope.launch {
            grid = repository.getPuzzle(Difficulty.MEDIUM)
            post()
        }
    }

    private fun post() = mutableViewState.postValue(GridViewState(grid, hints))

    fun answer(answerEvent: GridEvent.AnswerEvent) {
        grid.answers[answerEvent.position.first][answerEvent.position.second] = answerEvent.value
        post()
    }

    fun hint(hintEvent: GridEvent.HintEvent) {
        val hint = hints[hintEvent.position.first][hintEvent.position.second]

        when (hintEvent.value) {
            1 -> hint.showOne = !hint.showOne
            2 -> hint.showTwo = !hint.showTwo
            3 -> hint.showThree = !hint.showThree
            4 -> hint.showFour = !hint.showFour
            5 -> hint.showFive = !hint.showFive
            6 -> hint.showSix = !hint.showSix
            7 -> hint.showSeven = !hint.showSeven
            8 -> hint.showEight = !hint.showEight
        }

        post()
    }

    fun onEvent(event: GridEvent) {
        when (event) {
            is GridEvent.LoadEvent -> load()
            is GridEvent.AnswerEvent -> answer(event)
            is GridEvent.HintEvent -> hint(event)
        }
    }
}
