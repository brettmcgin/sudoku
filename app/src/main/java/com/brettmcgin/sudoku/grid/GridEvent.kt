package com.brettmcgin.sudoku.grid

sealed class GridEvent {
    object LoadEvent : GridEvent()
    data class AnswerEvent(val value: Int, val position: Pair<Int, Int>) : GridEvent()
    data class HintEvent(val value: Int, val position: Pair<Int, Int>) : GridEvent()
}
