package com.brettmcgin.sudoku.grid

enum class Difficulty(val difficulty: String) {
    EASY("easy"),
    MEDIUM("medium"),
    HARD("hard")
}
