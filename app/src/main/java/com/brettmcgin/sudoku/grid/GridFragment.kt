package com.brettmcgin.sudoku.grid

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.fragment.app.Fragment
import com.brettmcgin.sudoku.annotation.ApplicationScope
import com.brettmcgin.sudoku.databinding.AnsweredCellBinding
import com.brettmcgin.sudoku.databinding.CellGroupBinding
import com.brettmcgin.sudoku.databinding.FragmentGridBinding
import toothpick.ktp.KTP
import toothpick.ktp.delegate.inject
import toothpick.smoothie.lifecycle.closeOnDestroy

class GridFragment : Fragment() {
    private var _binding: FragmentGridBinding? = null
    @VisibleForTesting
    val binding
        get() = _binding!!

    private val gridViewModel: GridViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        injectDependencies()
    }

    @VisibleForTesting
    fun injectDependencies() = KTP.openScopes(ApplicationScope::class.java, this)
        .closeOnDestroy(this)
        .inject(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentGridBinding.inflate(inflater, container, false)

        gridViewModel.liveData.observe(
            this.viewLifecycleOwner,
            { gridViewState ->
                gridViewState.grid.answers.mapIndexed { rowIndex, row ->
                    row.mapIndexed { columnIndex, answer ->
                        val answeredCellBinding = getCell(rowIndex, columnIndex)
                        answeredCellBinding.cell.setOnClickListener {
                            gridViewModel.answer(
                                GridEvent.AnswerEvent(
                                    value = 10,
                                    Pair(rowIndex, columnIndex)
                                )
                            )
                        }

                        if (answer > 0) answeredCellBinding.cell.text = answer.toString()
                    }
                }
            }
        )

        return binding.root
    }

    private fun getGridBinding(rowIndex: Int, columnIndex: Int): CellGroupBinding {
        when {
            rowIndex < 3 -> when {
                columnIndex < 3 -> return binding.grid.groupTopLeft
                columnIndex < 6 -> return binding.grid.groupTop
                columnIndex < 9 -> return binding.grid.groupTopRight
            }
            rowIndex < 6 -> when {
                columnIndex < 3 -> return binding.grid.groupCenterLeft
                columnIndex < 6 -> return binding.grid.groupCenter
                columnIndex < 9 -> return binding.grid.groupCenterRight
            }
            rowIndex < 9 -> when {
                columnIndex < 3 -> return binding.grid.groupBottomLeft
                columnIndex < 6 -> return binding.grid.groupBottom
                columnIndex < 9 -> return binding.grid.groupBottomRight
            }
        }

        return binding.grid.groupBottomRight
    }

    private fun getCell(rowIndex: Int, columnIndex: Int): AnsweredCellBinding {
        val cellGroupBinding = getGridBinding(rowIndex, columnIndex)

        when (rowIndex % 3) {
            0 -> when (columnIndex % 3) {
                0 -> return cellGroupBinding.topLeft
                1 -> return cellGroupBinding.top
                2 -> return cellGroupBinding.topRight
            }
            1 -> when (columnIndex % 3) {
                0 -> return cellGroupBinding.centerLeft
                1 -> return cellGroupBinding.center
                2 -> return cellGroupBinding.centerRight
            }
            2 -> when (columnIndex % 3) {
                0 -> return cellGroupBinding.bottomLeft
                1 -> return cellGroupBinding.bottom
                2 -> return cellGroupBinding.bottomRight
            }
        }

        return cellGroupBinding.bottomRight
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
