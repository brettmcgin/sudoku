package com.brettmcgin.sudoku.grid

data class Grid(
    val answers: Array<Array<Int>> = arrayOf(arrayOf())
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Grid

        if (!answers.contentDeepEquals(other.answers)) return false

        return true
    }

    override fun hashCode(): Int {
        return answers.contentDeepHashCode()
    }
}
