package com.brettmcgin.sudoku.grid

data class Hint(
    var showOne: Boolean = false,
    var showTwo: Boolean = false,
    var showThree: Boolean = false,
    var showFour: Boolean = false,
    var showFive: Boolean = false,
    var showSix: Boolean = false,
    var showSeven: Boolean = false,
    var showEight: Boolean = false,
    var showNine: Boolean = false,
)
