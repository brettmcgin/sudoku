package com.brettmcgin.sudoku

import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        val textView = findViewById<TextView>(R.id.splash_title)

        textView.paint.shader = LinearGradient(
            0f,
            0f,
            textView.paint.measureText(getString(R.string.splash_title)),
            textView.textSize,
            arrayOf(
                Color.parseColor("#F97C3C"),
                Color.parseColor("#FDB54E"),
                Color.parseColor("#64B678"),
                Color.parseColor("#478AEA"),
                Color.parseColor("#8446CC")
            ).toIntArray(),
            null,
            Shader.TileMode.CLAMP
        )

        Handler(Looper.getMainLooper()).postDelayed(
            {
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))

                finish()
            },
            1200
        )
    }
}
