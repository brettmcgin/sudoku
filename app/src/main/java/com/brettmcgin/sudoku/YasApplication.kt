package com.brettmcgin.sudoku

import android.app.Application
import com.brettmcgin.sudoku.annotation.ApplicationScope
import com.brettmcgin.sudoku.grid.GridRepository
import com.brettmcgin.sudoku.sugoku.SugokuRepository
import toothpick.Scope
import toothpick.ktp.KTP
import toothpick.ktp.binding.bind
import toothpick.ktp.binding.module
import toothpick.smoothie.module.SmoothieApplicationModule

class YasApplication : Application() {
    lateinit var scope: Scope

    override fun onCreate() {
        super.onCreate()

        scope = KTP.openScope(ApplicationScope::class.java)
            .installModules(
                SmoothieApplicationModule(this),
                module {
                    bind<GridRepository>().toClass<SugokuRepository>()
                }
            )
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        scope.release()
    }
}
