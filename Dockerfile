FROM androidsdk/android-29 as build

WORKDIR /workdir

COPY . .

ENV GRADLE_OPTS='-Dorg.gradle.daemon=false -Dorg.gradle.parallel=true'

# Grabs latest gradle version, so container doesn't have to
RUN ./gradlew tasks

CMD ["./gradlew", "tasks"]
